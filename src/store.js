import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    testState: '测试vuex1'
  },
  mutations: {},
  actions: {},
  getters: {
    testGetter: state => state.testState
  }
})
