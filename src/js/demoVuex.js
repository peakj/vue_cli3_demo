const demoVuex = {
  state: {
    testDvuex: '一个新值'
  },
  getters: {
    getTestDvuex: state => state.testDvuex
  }
}
export default demoVuex
