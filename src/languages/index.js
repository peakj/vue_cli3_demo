import cn from './lib/cn'
import us from './lib/us'

export default {
  cn,
  us
}
