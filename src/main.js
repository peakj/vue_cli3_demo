import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'flex.css'
import VueI18n from 'vue-i18n'
import messages from './languages/index'
import ElementUI from 'element-ui'
Vue.config.productionTip = false
Vue.use(VueI18n)
Vue.use(ElementUI)
const i18n = new VueI18n({
  local: 'cn',
  fallbackLocale: 'cn',
  silentFallbackWarn: true,
  preserveDirectiveContent: true,
  messages: messages
})
// 热更新
if (module.hot) {
  module.hot.accept(['./languages/lib/us', './languages/lib/cn'], function() {
    i18n.setLocaleMessage('us', require('./languages/lib/us').default)
    i18n.setLocaleMessage('cn', require('./languages/lib/cn').default)
    // 同样可以通过 $i18n 属性进行热更新
    // app.$i18n.setLocaleMessage('en', require('./en').default)
    // app.$i18n.setLocaleMessage('ja', require('./ja').default)
  })
}
new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
